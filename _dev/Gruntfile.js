module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  let jsAutocomplete = [
    'node_modules/@tarekraafat/autocomplete.js/dist/autoComplete.js'
  ];

  let jsView = [
    'js/view/store-locator.js',
    'js/view/location-search.js',
    'js/view/view.js'
  ];

  const cssAutocomplete = [
    'tmp/autocomplete.css'
  ];

  let cssView = [
    'tmp/view.css'
  ];

  // noinspection JSUnresolvedFunction
  grunt.initConfig({
    makepot: {
      main: {
        options: {
          cwd: '../',
          mainFile: 'sdv-store-locator.php',
          domainPath: 'locales',
          exclude: ['_dev'],
          processPot: function (pot) {
            delete pot['translations']['']['https://www.gff.ch'];
            delete pot['translations']['']['GFF Integrative Kommunikation GmbH'];
            delete pot['translations']['']['Settings'];
            delete pot['translations']['']['Monday'];
            delete pot['translations']['']['Tuesday'];
            delete pot['translations']['']['Wednesday'];
            delete pot['translations']['']['Thursday'];
            delete pot['translations']['']['Friday'];
            delete pot['translations']['']['Saturday'];
            delete pot['translations']['']['Sunday'];
            delete pot['translations']['']['Mon'];
            delete pot['translations']['']['Tue'];
            delete pot['translations']['']['Wed'];
            delete pot['translations']['']['Thu'];
            delete pot['translations']['']['Fri'];
            delete pot['translations']['']['Sat'];
            delete pot['translations']['']['Sun'];

            return pot;
          }
        }
      }
    }
    ,
    uglify: {
      options: {
        mangle: true,
        compress: true
      },
      jsAutocomplete: {files: {'../scripts/autocomplete.js': jsAutocomplete}},
      jsView: {files: {'../scripts/view.js': jsView}}
    }
    ,
    sass: {
      options: {
        trace: false,
        sourcemap: 'none',
        style: 'compressed'
      },
      cssAutocomplete: {
        files: [{expand: true, cwd: 'scss/autocomplete', src: 'autocomplete.scss', dest: 'tmp', ext: '.css'}]
      },
      cssView: {
        files: [{expand: true, cwd: 'scss/view', src: 'view.scss', dest: 'tmp', ext: '.css'}]
      }
    }
    ,
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      cssAutocomplete: {files: {'tmp/autocomplete.complete.css': cssAutocomplete}},
      cssView: {files: {'tmp/view.complete.css': cssView}}
    }
    ,
    postcss: {
      options: {
        processors: [
          require('autoprefixer')
        ]
      },
      cssAutocomplete: {files: {'../styles/autocomplete.css': 'tmp/autocomplete.complete.css'}},
      cssView: {files: {'../styles/view.css': 'tmp/view.complete.css'}}
    }
    ,
    watch: {
      options: {spawn: false},
      jsAutocomplete: {files: jsAutocomplete, tasks: ['uglify:jsAutocomplete']},
      jsView: {files: jsView, tasks: ['uglify:jsView']},
      sassAll: {
        files: ['scss/**/*.scss'],
        tasks: ['sass', 'cssmin', 'postcss']
      }
    }
    ,
    compress: {
      main: {
        options: {
          archive: function () {
            return '../sdv-store-locator.zip';
          },
          mode: 'zip'
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!assets/*.xcf',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!README.md',
            ],
            dest: 'sdv-store-locator/'
          }
        ]
      }
    }
  });

  // Register the tasks.
  grunt.registerTask('default', ['uglify', 'sass', 'cssmin', 'postcss', 'watch']);
  grunt.registerTask('archive', ['compress']);
};
