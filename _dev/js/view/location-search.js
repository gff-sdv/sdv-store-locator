(function (storeLocator) {
  /** @var {object} google */

  let ctr = 0;

  const _maps = {};
  const _markers = {};
  const _listeners = {};
  const _infoWindows = {};

  const _getNextInstanceCounter = () => {
    return ctr++;
  };

  /**
   * Is Google Maps loaded?
   *
   * @returns {boolean}
   */
  const _googleMapsLoaded = () => {
    return (typeof google === 'object' && typeof google.maps === 'object');
  };

  /**
   * The svg icons repository.
   *
   * @type {{gpsLocation: (function(): object), icons: {}}}
   * @private
   */
  const _svgIcons = {
    icons: {},
    gpsLocation: () => {
      const name = 'gpsLocation';
      if (_svgIcons.icons[name] === undefined) {
        _svgIcons.icons[name] = {
          path: 'M 40.351269,19.351258 H 38.552732 C 37.948407,11.138683 30.844738,4.0352553 22.632498,3.4309299 V 1.6324732 c -0.08662,-2.17678546 -3.195368,-2.17514483 -3.281254,0 V 3.4309299 C 11.138757,4.0352553 4.0352523,11.138929 3.4309274,19.351258 H 1.632472 c -2.17678386,0.08662 -2.17514323,3.195369 0,3.281256 h 1.7984554 c 0.3749653,4.091563 2.148483,7.889125 5.0897991,10.830443 2.9413165,2.941318 6.7388755,4.714838 10.8304355,5.089803 v 1.798457 c 0.08663,2.176785 3.195367,2.175145 3.281254,0 V 38.55276 c 4.091559,-0.375047 7.889119,-2.148567 10.830435,-5.089803 2.941316,-2.9414 4.714834,-6.73888 5.089799,-10.830443 h 1.798455 c 2.176948,-0.08671 2.175225,-3.195451 1.64e-4,-3.281256 z M 20.991871,35.347383 c -7.915615,0 -14.3554866,-6.439876 -14.3554866,-14.355497 0.7885674,-19.0444942 27.9252766,-19.0389981 28.7109726,8.2e-5 0,7.915539 -6.439871,14.355415 -14.355486,14.355415 z m 0,-9.433612 c -2.713926,0 -4.921881,-2.207958 -4.921881,-4.921885 0.270375,-6.529536 9.574371,-6.527649 9.843762,0 0,2.713927 -2.207956,4.921885 -4.921881,4.921885 z m 0,-6.562513 c -0.904642,0 -1.640627,0.735986 -1.640627,1.640628 0.09007,2.176539 3.191511,2.175883 3.281254,0 0,-0.904642 -0.735986,-1.640628 -1.640627,-1.640628 z',
          fillColor: '#1dbbe2',
          fillOpacity: 0.5,
          strokeWeight: 0,
          anchor: new google.maps.Point(24, 24),
        };
      }

      return _svgIcons.icons[name];
    }
  };

  const _addMarkerAnchor = (marker, anchor) => {
    const map = marker.getMap();
    const mapElementId = _getMapElementId(map);

    _listeners[mapElementId].push(
      marker.addListener('click', () => {
        const target = document.getElementById(anchor);
        [].forEach.call(
          target.closest('[data-results]').querySelectorAll('.active'),
          element => {
            element.classList.remove('active');
          });

        target.classList.add('active');
        target.scrollIntoView();
      })
    );
  };

  /**
   * Gets the Google Maps map.
   *
   * @param {Element} mapElement
   * @returns {google.maps.Map}
   * @private
   */
  const _getMap = mapElement => {
    const mapElementId = mapElement.id;

    if (_maps[mapElementId] === undefined) {
      // We need to initialize the map.
      mapElement.style.height = '400px';

      _maps[mapElementId] = new google.maps.Map(
        mapElement,
        {
          zoom: 15,
          styles: [
            {featureType: 'poi', stylers: [{visibility: 'off'}]},
            {featureType: 'transit', elementType: 'labels.icon', stylers: [{visibility: 'off'}]}
          ]
        }
      );

      _markers[mapElementId] = [];
      _listeners[mapElementId] = [];
      _infoWindows[mapElementId] = [];
    }

    return _maps[mapElementId];
  };

  /**
   * Clears the map.
   *
   * @param {google.maps.Map} map
   * @private
   */
  const _clearMap = (map) => {
    const mapElementId = map.getDiv().id;

    // Remove any markers associated with this map.
    while (_markers[mapElementId].length > 0) {
      const marker = _markers[mapElementId].shift();
      // noinspection JSUnresolvedFunction
      marker.setMap(null);
    }

    // Remove any listeners.
    while (_listeners[mapElementId].length > 0) {
      const listener = _listeners[mapElementId].shift();
      listener.remove();
    }

    // Close all info windows.
    while (_infoWindows[mapElementId].length > 0) {
      const infoWindow = _infoWindows[mapElementId].shift();
      infoWindow.close();
    }
  };

  /**
   * Gets the map element id given the map.
   *
   * @param {google.maps.Map} map
   * @return {string}
   * @private
   */
  const _getMapElementId = (map) => {
    return map.getDiv().id;
  };

  /**
   * Adds a gps-location marker to the given map at the given position.
   *
   * @param {google.maps.Map} map
   * @param {google.maps.LatLng} position
   * @returns {google.maps.Marker}
   * @private
   */
  const _addGpsLocationMarker = (map, position) => {
    return _addMarker(map, {
      position: position,
      icon: _svgIcons.gpsLocation()
    });
  };

  /**
   * Adds a store marker to the given map at the given position.
   *
   * @param {google.maps.Map} map
   * @param {google.maps.LatLng} position
   * @param {string} title
   * @returns {google.maps.Marker}
   * @private
   */
  const _addResultMarker = (map, position, title) => {
    return _addMarker(map, {
      position: position,
      animation: google.maps.Animation.DROP,
      title: title
      // collisionBehavior: google.maps.CollisionBehavior.REQUIRED_AND_HIDES_OPTIONAL
    });
  };

  /**
   * Low-level method to add a marker to a map.
   *
   * @param {google.maps.Map} map
   * @param {object} options
   * @returns {google.maps.Marker}
   * @private
   */
  const _addMarker = (map, options) => {
    const mapElementId = _getMapElementId(map);
    options['map'] = map;

    const marker = new google.maps.Marker(options);
    _markers[mapElementId].push(marker);

    return marker;
  };

  // /**
  //  * Adds an info window to be used by a marker.
  //  *
  //  * @param {google.maps.Marker} marker
  //  * @param {string} contents
  //  * @private
  //  */
  // const _addInfoWindow = (marker, contents) => {
  //   const map = marker.getMap();
  //   const mapElementId = _getMapElementId(map);
  //
  //   const infoWindow = new google.maps.InfoWindow({
  //     content: contents
  //   });
  //   _infoWindows[mapElementId].push(infoWindow);
  //
  //   _listeners[mapElementId].push(
  //     marker.addListener('click', () => {
  //       _infoWindows[mapElementId].forEach(w => {
  //         w.close();
  //       });
  //
  //       infoWindow.open(map, marker);
  //     })
  //   );
  // };

  /**
   * Initializes a Location Search element.
   *
   * @param element
   * @param buildSearchUrlCallback
   * @param renderResultCallback
   * @param renderResultParams
   * @private
   */
  const _init = (element, buildSearchUrlCallback, renderResultCallback, renderResultParams) => {
    const name = 'gps-autocomplete-' + _getNextInstanceCounter();
    console.debug(name);

    // Get the autocomplete input element.
    const autocomplete_input = element.querySelector('.autocomplete-wrapper input');
    if (autocomplete_input === undefined) {
      console.error('Location Search: Autocomplete Input Control could not be found.');
      return;
    }

    // Get the autocomplete source url (for ajax requests).
    const url = autocomplete_input.getAttribute('data-autocomplete-url');

    // noinspection JSPotentiallyInvalidConstructorUsage
    new autoComplete({
      name: name,
      selector: () => {
        return autocomplete_input;
      },
      data: {
        src: async function () {
          const source = await fetch(url + '&q=' + encodeURIComponent(autocomplete_input.value));

          return await source.json();
        },
        keys: ['label']
      },
      threshold: 2,
      debounce: 300,
      searchEngine: function (query, record) {return record;},
      resultsList: {
        class: 'autocomplete-list',
        tabSelect: true,
        maxResults: 10,
        noResults: true
      }
    });

    autocomplete_input.addEventListener('selection', function (event) {
      const value = event.detail['selection']['value'];

      autocomplete_input.value = value['label'];
      autocomplete_input.blur();

      const search_url = buildSearchUrlCallback(element, value['lat'], value['lng']);
      if (search_url === null) {
        console.error('Search url is undefined.');
        return;
      }

      // noinspection JSIgnoredPromiseFromCall
      _runSearch(element, name, search_url, value['lat'], value['lng'], renderResultCallback, renderResultParams);
    });
  };

  /**
   * Runs a search.
   *
   * @param {Element} element
   * @param {string} gps_autocomplete_uid
   * @param {string} search_url
   * @param {number} lat
   * @param {number} lng
   * @param {function} renderResultCallback
   * @param {Array} renderResultParams
   * @returns {Promise<void>}
   * @private
   */
  const _runSearch = async (element, gps_autocomplete_uid, search_url, lat, lng, renderResultCallback, renderResultParams) => {
    // Get the map element.
    const mapElement = element.querySelector('[data-map]');
    if (mapElement === null) {
      return;
    }

    if (!_googleMapsLoaded()) {
      // Google Maps API is not loaded.
      // TODO The result list might be shown anyhow.
      return;
    }

    // Get the Google Maps map.
    const map = _getMap(mapElement);

    // Clear the map (remove markers, info windows, event listeners).
    _clearMap(map);

    // Set the center of the map to be the current latitude, longitude.
    const center = new google.maps.LatLng(lat, lng);
    map.setCenter(center);
    map.setZoom(15);

    _addGpsLocationMarker(map, center);

    // Initialize the bounds.
    const bounds = new google.maps.LatLngBounds();
    bounds.extend(center);

    // Get the results.
    const response = await fetch(search_url);
    const json = await response.json();

    let results = [];
    let resultCounter = 0;
    json['results'].forEach(result => {
      const resultId = gps_autocomplete_uid + '-' + resultCounter;

      const latLng = new google.maps.LatLng(result['lat'], result['lng']);

      results.push(
        '<li id="' + resultId + '">' +
        renderResultCallback(result, renderResultParams) +
        '</li>'
      );

      const marker = _addResultMarker(map, latLng, result['name']);
      bounds.extend(latLng);

      _addMarkerAnchor(marker, resultId);

      // _addInfoWindow(marker, '<div>' +
      //   '<div><p><strong>' + result['name'] + '</strong></p></div>' +
      //   '<div>' + result['contact'] + '</div>' +
      //   '</div>');

      resultCounter++;
    });

    const results_element = element.querySelector('[data-results]');
    results_element.innerHTML = '<ul>' + results.join('') + '</ul>';

    // Don't zoom in too far on only one marker!
    const northEast = bounds.getNorthEast();
    if (northEast.equals(bounds.getSouthWest())) {
      const extension = 0.003;

      const northEastLat = northEast.lat();
      const northEastLng = northEast.lng();

      const extendPoint1 = new google.maps.LatLng(northEastLat + extension, northEastLng + extension);
      const extendPoint2 = new google.maps.LatLng(northEastLat - extension, northEastLng - extension);

      bounds.extend(extendPoint1);
      bounds.extend(extendPoint2);
    }

    map.fitBounds(bounds);
  };

  storeLocator.LocationSearch = {
    init: _init
  };
})(StoreLocator);