if (document.readyState !== 'loading') {
  StoreLocator.init();
} else {
  document.addEventListener('DOMContentLoaded', StoreLocator.init);
}
