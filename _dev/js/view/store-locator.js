const StoreLocator = (function () {
  /**
   * Initializes all store locators on the current document.
   *
   * @private
   */
  const _init = () => {
    [].forEach.call(document.querySelectorAll('.store-locator'), _initStoreLocator);
  };

  /**
   * Initializes a given store locator.
   *
   * @param {Element} element
   *
   * @private
   */
  const _initStoreLocator = (element) => {
    const openingHoursTitle = element.getAttribute('data-opening-hours');

    StoreLocator.LocationSearch.init(
      element,
      function (store_locator, lat, lng) {
        const search_url = store_locator.getAttribute('data-search-url');
        if (search_url === null) {
          // The search url has to be defined as data attribute.
          return null;
        }

        return search_url + '&lat=' + lat + '&lng=' + lng;
      },
      function (result) {
        let openingHours = '<strong>' + openingHoursTitle + '</strong><br>';
        if (typeof result['openingHours'] === 'string') {
          openingHours += result['openingHours'];
        } else {
          result['openingHours'].forEach(item => {
            openingHours += item['day'] + ': ' + item['times'] + '<br>';
          });
        }

        return '<div class="store-locator-result-contact">' +
          '<p class="name">' + result['name'] + '</p>' +
          result['contact'] +
          '</div>' +
          '<div class="store-locator-result-opening-hours">' +
          '<p>' + openingHours + '</p>' +
          '</div>';
      }, {
        'opening_hours_heading': element.getAttribute('data-opening-hours')
      }
    );
  };

  return {
    init: _init
  };
})();
