<?php

/*
Plugin Name: SDV Store Locator
Description: GFF SDV store locator to find drugstores.
Version: 1.10
Author: GFF Integrative Kommunikation GmbH
Author URI: https://www.gff.ch
License: Proprietary
Text Domain: store-locator
Domain Path: /locales
*/

declare( strict_types=1 );

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Store Locator Version.
 */
define( 'SDV_STORE_LOCATOR_VERSION', '1.10' );

/**
 * Absolute filesystem path to the plugin root file.
 */
define( 'SDV_STORE_LOCATOR_ABS_PLUGIN_FILE', __FILE__ );

/**
 * Absolute filesystem path to the plugin.
 */
define( 'SDV_STORE_LOCATOR_ABS_PATH', dirname( __FILE__ ) );

/**
 * Root url of the plugin.
 */
define( 'SDV_STORE_LOCATOR_ROOT_URL', plugin_dir_url( __FILE__ ) );

/**
 * Check dependencies and register update checker.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'sdv-vendor/sdv-vendor.php' ) ) {
	include_once( SDV_STORE_LOCATOR_ABS_PATH . '/includes/bootstrap.php' );
} else {
	add_action( 'admin_notices', function () {
		$class   = 'notice notice-error';
		$message = __(
			'SDV Store Locator needs the SDV Vendor plugin to be installed and activated.',
			'store-locator'
		);

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	} );
}
