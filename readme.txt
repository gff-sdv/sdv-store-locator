=== SDV Store Locator ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 6.5
Tested up to: 6.7
Requires PHP: 7.4
Stable tag: 1.10
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides the search for drugstores.

== Description ==

This plugin provides GFF SDV drugstore search.

== Changelog ==

= 1.10 =
* Fix geo admin endpoint to get zip codes, location names and coordinates.

= 1.9 =
* WordPress compatibility.

= 1.8 =
* Deduplicate search result.

= 1.7 =
* Fix geo admin endpoint to get zip codes and location names.

= 1.6 =
* Bugfixes.

= 1.3 =
* Put Location Search javascript into separate namespace.

= 1.2 =
* Minor bug fixes.
