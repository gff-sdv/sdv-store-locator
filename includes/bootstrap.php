<?php

declare( strict_types=1 );

use SDV_Store_Locator\Store_Locator;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Gets the current Store_Locator instance. If there is currently no instance,
 * this function would instantiate one.
 *
 * @return Store_Locator The current instance.
 */
function sdv_store_locator(): Store_Locator {
	static $instance = null;

	if ( is_null( $instance ) ) {
		$instance = new Store_Locator();
	}

	return $instance;
}

////////////////////////////// PHP CLASS AUTOLOAD //////////////////////////////

/**
 * The autoload function for `SDV_Store_Locator` classes.
 *
 * Note that the namespaces for these classes must never be prefixed by `\`.
 */
spl_autoload_register( function ( string $class_fqn ): void {
	static $base_path = SDV_STORE_LOCATOR_ABS_PATH . '/includes/';

	// Get the first characters of the class fqn as they would be checked afterwards.
	$class_fqn_begin = substr( $class_fqn, 0, 18 );
	if ( 'SDV_Store_Locator\\' !== $class_fqn_begin ) {
		return;
	}

	/** @noinspection DuplicatedCode */
	$p = strripos( $class_fqn, '\\' );
	if ( false === $p ) {
		$namespace  = '';
		$class_name = $class_fqn;
	} else {
		$namespace  = substr( $class_fqn, 0, $p );
		$class_name = substr( $class_fqn, $p + 1 );
	}

	$class_file = 'class-' . str_replace( '_', '-', strtolower( $class_name ) ) . '.php';
	$class_path = $base_path . (
		! empty( $namespace ) ?
			str_replace( [ '_', '\\' ], [ '-', '/' ], strtolower( $namespace ) ) . '/' :
			''
		) . $class_file;

	if ( is_file( $class_path ) ) {
		include_once( $class_path );
	} else {
		trigger_error( 'Class <b>' . $class_fqn . '</b> not found.', E_USER_ERROR );
	}
} );

////////////////////////// Plugin //////////////////////////

sdv_store_locator();

////////////////////// Update Checker //////////////////////

add_action( 'plugins_loaded', function () {
	$updateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/gff-sdv/sdv-store-locator/',
		SDV_STORE_LOCATOR_ABS_PLUGIN_FILE,
		'sdv-store-locator'
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcsApi */
	$vcsApi = $updateChecker->getVcsApi();
	$vcsApi->enableReleasePackages();
} );
