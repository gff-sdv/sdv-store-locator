<?php

declare( strict_types=1 );

namespace SDV_Store_Locator;

class Search_API {
	protected const ACTION_NAME = 'store_locator_search';

	public function __construct() {
		add_action( 'wp_ajax_' . self::ACTION_NAME, [ $this, 'ajax_callback' ] );
		add_action( 'wp_ajax_nopriv_' . self::ACTION_NAME, [ $this, 'ajax_callback' ] );
	}

	public function ajax_callback() {
		check_ajax_referer( self::ACTION_NAME, '_nonce' );

		$lat = filter_input( INPUT_GET, 'lat' );
		$lng = filter_input( INPUT_GET, 'lng' );

		$weekdays = [
			[ 'label' => __( 'Mon' ), 'nr' => 1 ],
			[ 'label' => __( 'Tue' ), 'nr' => 2 ],
			[ 'label' => __( 'Wed' ), 'nr' => 3 ],
			[ 'label' => __( 'Thu' ), 'nr' => 4 ],
			[ 'label' => __( 'Fri' ), 'nr' => 5 ],
			[ 'label' => __( 'Sat' ), 'nr' => 6 ],
			[ 'label' => __( 'Sun' ), 'nr' => 0 ],
		];

		$response = wp_remote_get(
			'https://directory.vitagate.ch/jsonSearch.php?lang=de&q=' . $lat . ',' . $lng,
			[ 'timeout' => 10 ]
		);
		$results  = [];
		if ( is_array( $response ) && array_key_exists( 'body', $response ) ) {
			$data = json_decode( $response['body'], true );
			if ( json_last_error() === JSON_ERROR_NONE ) {
				foreach ( $data as $item ) {
					if ( ! empty( $item['openingHours'] ) ) {
						$openingHours = [];
						foreach ( $weekdays as $weekday ) {
							$times = '';

							$hours = $item['openingHours'][ $weekday['nr'] ];
							if ( count( $hours ) < 2 ) {
								$times = __( 'closed', 'store-locator' );
							} else {
								for ( $i = count( $hours ) - 2; $i >= 0; $i -- ) {
									if ( $hours[ $i ] === $hours[ $i + 1 ] ) {
										array_splice( $hours, $i, 2 );
									}
								}
//								for ( $i = 1, $c = count( $hours ); $i < $c; $i += 1 ) {
//									if ( $hours[ $i ] === $hours[ $i - 1 ] ) {
//										array_splice( $hours, $i - 1, 2 );
//										$i -= 2;
//										$c -= 2;
//									}
//								}

								for ( $i = 1, $c = count( $hours ); $i < $c; $i += 2 ) {
									$times .= ( ! empty( $times ) ? ', ' : '' ) . $hours[ $i - 1 ] . '–' . $hours[ $i ];
								}
							}
							array_push( $openingHours, [ 'day' => $weekday['label'], 'times' => $times ] );
						}
					} else {
						$openingHours = __( 'No opening hours available.', 'store-locator' );
					}

					array_push( $results, [
						'id'           => $item['id'],
						'lat'          => $item['lat'],
						'lng'          => $item['lng'],
						'name'         => $item['name'],
						'contact'      => $item['contact'],
						'website'      => $item['website'],
						'openingHours' => $openingHours
					] );
				}
			}
		}

		wp_send_json( [ 'lat' => $lat, 'lng' => $lng, 'results' => $results ] );
	}

	/**
	 * Creates a nonce for this API action.
	 *
	 * @return string
	 */
	public function create_nonce(): string {
		return wp_create_nonce( self::ACTION_NAME );
	}

	public function get_action(): string {
		return self::ACTION_NAME;
	}
}
