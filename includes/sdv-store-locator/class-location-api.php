<?php

declare( strict_types=1 );

namespace SDV_Store_Locator;

class Location_API {

	protected const ACTION_NAME = 'store_locator_location';

	protected Location_Search $location_search;

	/**
	 * Constructor.
	 *
	 * @param Location_Search $location_search
	 */
	public function __construct( Location_Search $location_search ) {
		add_action( 'wp_ajax_' . self::ACTION_NAME, [ $this, 'ajax_callback' ] );
		add_action( 'wp_ajax_nopriv_' . self::ACTION_NAME, [ $this, 'ajax_callback' ] );

		$this->location_search = $location_search;
	}

	/**
	 * Ajax callback function.
	 *
	 * @return void
	 */
	public function ajax_callback(): void {
		check_ajax_referer( self::ACTION_NAME, '_nonce' );

		$query = trim( filter_input( INPUT_GET, 'q' ) );

		wp_send_json( $this->location_search->search( $query, 20 ) );
	}

	/**
	 * @return string
	 */
	public function create_nonce(): string {
		return wp_create_nonce( self::ACTION_NAME );
	}

	/**
	 * @return string
	 */
	public function get_action(): string {
		return self::ACTION_NAME;
	}
}
