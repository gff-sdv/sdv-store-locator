<?php

declare( strict_types=1 );

namespace SDV_Store_Locator;

class Location_Search {
	/**
	 * @param string $query
	 * @param int    $limit
	 *
	 * @return array
	 */
	public function search( string $query, int $limit = 20 ): array {
		$geo_locations = get_option( 'geo_locations', [] );
		if ( ! is_array( $geo_locations ) ) {
			$geo_locations = [];
		}

		$zip_code = intval( $query );
		if ( $zip_code > 0 ) {
			// Search by zip code (ignore the rest of the query).
			$results = $this->search_by_zip_code( $zip_code, $geo_locations );
		} else {
			// Search by name (ignore special chars).
			$results = $this->search_by_name( $query, $geo_locations );
		}

		// The imported geolocations from `admin.ch` would contain duplicates (some zip codes are part of multiple communes).
		// We filter such that we would only consider the first match.
		$data   = [];
		$labels = [];
		foreach ( $results as $result ) {
			if ( count( $data ) >= $limit ) {
				break;
			}

			$label = $result[0] . ' ' . $result[1];
			if ( in_array( $label, $labels ) ) {
				continue;
			}

			$labels[] = $label;
			$data[]   = [
				'label' => $label,
				'lng'   => $result[2],
				'lat'   => $result[3],
			];
		}

		return $data;
	}

	/**
	 * @param int   $zip_code
	 * @param array $geo_locations
	 *
	 * @return array
	 */
	protected function search_by_zip_code( int $zip_code, array $geo_locations ): array {
		$m            = pow( 10, 3 - floor( log( $zip_code, 10 ) ) );
		$zip_code_min = floor( $zip_code * $m );
		$zip_code_max = floor( ( $zip_code + 1 ) * $m - 1 );

		$results = array_filter( $geo_locations, function ( $item ) use ( $zip_code_min, $zip_code_max ) {
			return $item[0] >= $zip_code_min && $item[0] <= $zip_code_max;
		} );

		usort( $results, function ( $a, $b ) {
			if ( $a[0] > $b[0] ) {
				return 1;
			}
			if ( $a[0] < $b[0] ) {
				return - 1;
			}

			return 0;
		} );

		return $results;
	}

	/**
	 * @param string $query
	 * @param array  $geo_locations
	 *
	 * @return array
	 */
	protected function search_by_name( string $query, array $geo_locations ): array {
		$terms        = array_filter( preg_split( '/(?:\d|\s|,|-|\.)+/', $query ) );
		$count_terms  = count( $terms );
		$term_lengths = array_map( function ( $item ) {
			return mb_strlen( $item );
		}, $terms );

		$scores = [];
		for ( $i = 0, $c = count( $geo_locations ); $i < $c; $i ++ ) {
			$score         = 0;
			$matched_terms = 0;

			// Get the length of the geolocation name.
			$length = mb_strlen( $geo_locations[ $i ][1] );

			// Iterate over all terms and calculate a score for each term. The score would be between 0 and 1.
			//
			// We use the following rules:
			// L: Length of haystack
			// M: Length of needle
			// P: Position of needle in haystack
			// W: Weight
			// S: Score
			//
			// S = W * (M / L) + (1 - W) * (1 - P / (L - M + 1))
			//
			// So if the needle is almost as long as the haystack, then this value gets close to 1. If the needle
			// is very short compared to the haystack, then this value gets close to 0. The score would be bigger
			// if the needle is at the beginning of the haystack.
			//
			$w = 0.5;
			for ( $j = 0; $j < $count_terms; $j ++ ) {
				if ( ( $p = mb_stripos( $geo_locations[ $i ][1], $terms[ $j ] ) ) !== false ) {
					$m     = $term_lengths[ $j ];
					$score += $w * ( $m / $length ) + ( 1 - $w ) * ( 1 - $p / ( $length - $m + 1 ) );
					$matched_terms ++;
				}
			}

			if ( $score > 0 ) {
				// If the query is exactly as the haystack, then we add a big score.
				if ( mb_strtolower( $geo_locations[ $i ][1] ) === mb_strtolower( $query ) ) {
					$score += $count_terms;
				}
			}

			if ( $score > 0 ) {
				// Add the zip code (but only as a very little fraction) so that two results with the same "score"
				// would be ordered by zip code.
				$score += 1 - $geo_locations[ $i ][0] / 100000;
			}

			$scores[ $i ] = $score;
		}

		$results = [];
		arsort( $scores );
		foreach ( $scores as $index => $score ) {
			if ( $score === 0 ) {
				break;
			}
			$results[] = $geo_locations[ $index ];
		}

		return $results;
	}
}
