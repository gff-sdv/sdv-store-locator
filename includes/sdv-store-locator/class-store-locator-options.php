<?php

declare( strict_types=1 );

namespace SDV_Store_Locator;

use ZipArchive;

class Store_Locator_Options {

	const ACTION_IMPORT_LOCATIONS = 'store_locator_import_locations';

	const SLUG = 'store_locator_locations';

	const OPTION_PAGE = 'store-locator-page';

	const OPTION_GROUP = 'store-locator';

	const OPTION_NAME = 'store-locator';

	const SECTION_ID = 'store-locator-section';

	const SETTING_GOOGLE_MAPS_API_KEY = 'google-maps-api-key';

	protected Location_Search $location_search;

	/**
	 * Constructor.
	 *
	 * @param Location_Search $location_search
	 */
	public function __construct( Location_Search $location_search ) {
		$this->location_search = $location_search;

		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'admin_init', [ $this, 'admin_init' ] );
	}

	/**
	 * Hook `admin_menu` handler.
	 */
	public function admin_menu(): void {
		add_options_page(
			__( 'Store Locator', 'store-locator' ),
			__( 'Store Locator', 'store-locator' ),
			'manage_options',
			self::SLUG,
			[ $this, 'settings_page' ]
		);
	}

	/**
	 * Hook `admin_init` handler.
	 */
	public function admin_init(): void {
		register_setting( self::OPTION_GROUP, self::OPTION_NAME );

		add_settings_section(
			self::SECTION_ID,
			__( 'Store Locator', 'store-locator' ),
			[ $this, 'section_callback' ],
			self::OPTION_PAGE
		);

		add_settings_field(
			self::SETTING_GOOGLE_MAPS_API_KEY,
			__( 'Google Maps API Key', 'store-locator' ),
			[ $this, 'google_maps_api_key_render' ],
			self::OPTION_PAGE,
			self::SECTION_ID,
			[
				'label_for' => self::SETTING_GOOGLE_MAPS_API_KEY,
			]
		);
	}

	/**
	 * Gets all options.
	 *
	 * @return array<string, mixed>
	 */
	public function get_options(): array {
		$options = get_option( self::OPTION_NAME );

		return array_merge(
			[
				self::SETTING_GOOGLE_MAPS_API_KEY => '',
			],
			is_array( $options ) ? $options : []
		);
	}

	/**
	 * @return string
	 */
	public function get_option_google_maps_api_key(): string {
		$options = $this->get_options();

		return $options[ self::SETTING_GOOGLE_MAPS_API_KEY ];
	}

	/**
	 * Section callback.
	 */
	public function section_callback(): void {
	}

	/**
	 * Render the input control for the Google Maps API Key.
	 */
	public function google_maps_api_key_render(): void {
		echo '<input id="' . self::SETTING_GOOGLE_MAPS_API_KEY . '" type="text" class="regular-text" name="' . self::OPTION_NAME . '[' . self::SETTING_GOOGLE_MAPS_API_KEY . ']" value="' . esc_attr( $this->get_option_google_maps_api_key() ) . '">';
	}

	/**
	 * Settings page display callback.
	 */
	public function settings_page(): void {
		echo '<div class="wrap"><h1>' .
		     __( 'Settings', 'default' ) . ' › ' .
		     __( 'Store Locator', 'store-locator' ) .
		     '</h1>';

		$this->settings_page_gps_locations();

		/** @noinspection HtmlUnknownTarget */
		echo '<form action="options.php" method="post">';
		settings_fields( self::OPTION_GROUP );
		do_settings_sections( self::OPTION_PAGE );
		submit_button();
		echo '</form>';

		echo '</div>';
	}

	protected function settings_page_gps_locations(): void {
		echo '<h2>' . __( 'GPS Locations', 'store-locator' ) . '</h2>';

		if ( filter_input( INPUT_POST, 'action' ) === self::ACTION_IMPORT_LOCATIONS ) {
			check_admin_referer( self::ACTION_IMPORT_LOCATIONS );

			$uri = 'https://data.geo.admin.ch/ch.swisstopo-vd.ortschaftenverzeichnis_plz/ortschaftenverzeichnis_plz/ortschaftenverzeichnis_plz_4326.csv.zip';
			$response = wp_remote_get( $uri );
			if ( is_string( $response['body'] ) ) {
				$zipPath = wp_tempnam( self::SLUG );
				file_put_contents( $zipPath, $response['body'] );
				register_shutdown_function( function () use ( $zipPath ) {
					@unlink( $zipPath );
				} );

				$zip = new ZipArchive();
				if ( $zip->open( $zipPath ) === true ) {
					$stream = $zip->getStream( 'AMTOVZ_CSV_WGS84/AMTOVZ_CSV_WGS84.csv' );
					if ( is_resource( $stream ) ) {
						$geo_locations = [];

						$lineNr = 0;
						while ( ( $line = fgets( $stream, 4096 ) ) !== false ) {
							$lineNr ++;
							if ( $lineNr === 1 ) {
								continue;
							}

							// $line = mb_convert_encoding( $line, 'UTF-8', 'ISO-8859-1' );
							$data = str_getcsv( $line, ';' );

							$geo_locations[] = [
								/* ZipCode */
								intval( $data[1] ),
								/* LocationName */
								$data[0],
								/* Longitude */
								floatval( $data[6] ),
								/* Latitude */
								floatval( $data[7] ),
							];
						}
						update_option( 'geo_locations', $geo_locations, false );
						update_option( 'geo_locations_timestamp', time(), false );

						if ( ! feof( $stream ) ) {
							echo '<p>Error: Unexpected failure.</p>';
						}
						fclose( $stream );
					} else {
						echo '<p>Sorry, I could not open the csv file inside the zip archive.</p>';
					}

					$zip->close();
				} else {
					echo '<p>Zip could not be opened.</p>';
				}
			} else {
				echo '<p>Response body from «' . htmlspecialchars( $uri ) . '» was invalid.</p>';
			}
		}

		$geo_locations_timestamp = get_option( 'geo_locations_timestamp', null );
		if ( ! is_null( $geo_locations_timestamp ) ) {
			printf(
				'<p>' . __( 'Last refresh: %s', 'store-locator' ) . '</p>',
				date_i18n(
					get_option( 'date_format' ) . ' ' . get_option( 'time_format' ),
					$geo_locations_timestamp
				)
			);
		}

		$geo_locations = get_option( 'geo_locations', [] );
		printf(
			'<p>' . __( 'Number of geo locations in the database: %s', 'store-locator' ) . '</p>',
			number_format_i18n( count( $geo_locations ) )
		);

		///
		// Perform live search tests.
		///
		$successes = 0;
		$failures  = [];
		$tests     = [
			[
				'zürich',
				function ( $results ): int {
					if ( ! empty( $results ) && stripos( $results[0]['label'], 'zürich' ) !== false ) {
						return 0;
					}

					return 1;
				}
			],
			[
				'2502',
				function ( $results ): int {
					if ( ! empty( $results ) && stripos( $results[0]['label'], 'biel' ) !== false ) {
						return 0;
					}

					return 1;
				}
			],
			[
				'del',
				function ( $results ): int {
					foreach ( $results as $result ) {
						if ( stripos( $result['label'], 'delémont' ) !== false ) {
							return 0;
						}
					}

					return 1;
				}
			]
		];

		foreach ( $tests as $test ) {
			$results = $this->location_search->search( $test[0], 50 );
			$code    = call_user_func( $test[1], $results );
			if ( $code === 0 ) {
				$successes ++;
			} else {
				$failures[] = [ $test[0], $code ];
			}
		}

		echo '<p>' . $successes . ' of ' . count( $tests ) . ' tests were successful.</p>';
		if ( ! empty( $failures ) ) {
			echo '<p>Failures:</p>';
			echo '<ul>';
			foreach ( $failures as $failure ) {
				echo '<li>' . htmlspecialchars( $failure[0] ) . ' => ' . $failure[1] . '</li>';
			}
			echo '</ul>';
		}

		///
		// Form that allows importing geolocation data.
		///
		$action_url = add_query_arg( [ 'page' => self::SLUG ], admin_url( 'options-general.php' ) );

		echo '<form action="' . $action_url . '" method="post">
<input type="hidden" name="action" value="' . self::ACTION_IMPORT_LOCATIONS . '">
<input type="hidden" name="_wpnonce" value="' . wp_create_nonce( self::ACTION_IMPORT_LOCATIONS ) . '">
<input type="submit" name="submit"  class="button button-primary" value="' .
		     esc_attr__( 'Import geo locations from data.geo.admin.ch', 'store-locator' ) . '">
</form>';
	}
}
