<?php

declare( strict_types=1 );

namespace SDV_Store_Locator;

class Store_Locator {

	protected Location_Search $location_search;

	protected Location_API $location_api;

	protected Search_API $search_api;

	protected Shortcode $shortcode;

	protected Store_Locator_Options $options;

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->location_search = new Location_Search();
		$this->location_api    = new Location_API( $this->location_search );
		$this->search_api      = new Search_API();
		$this->options         = new Store_Locator_Options( $this->location_search );
		$this->shortcode       = new Shortcode( $this->options );

		// Set the `plugins_loaded` action handler (note that this hook gets called before the init hook).
		add_action( 'plugins_loaded', [ $this, 'plugins_loaded' ] );

		// Set the `init` action handler.
		add_action( 'init', [ $this, 'init' ] );
	}

	/**
	 * @return Location_Search
	 */
	public function get_location_search(): Location_Search {
		return $this->location_search;
	}

	/**
	 * @return Location_API
	 */
	public function get_location_api(): Location_API {
		return $this->location_api;
	}

	/**
	 * @return Search_API
	 */
	public function get_search_api(): Search_API {
		return $this->search_api;
	}

	/**
	 * @return Shortcode
	 */
	public function get_shortcode(): Shortcode {
		return $this->shortcode;
	}

	/**
	 * @return Store_Locator_Options
	 */
	public function get_options(): Store_Locator_Options {
		return $this->options;
	}

	/**
	 * Hook `plugins_loaded` handler.
	 */
	public function plugins_loaded(): void {
		load_plugin_textdomain( 'store-locator', false, 'sdv-store-locator/locales' );
	}

	/**
	 * Hook `init` handler.
	 */
	public function init(): void {
		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'wp_enqueue_scripts' ] );
	}

	/**
	 * Hook `admin_init` handler.
	 */
	public function admin_init(): void {
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
	}

	/**
	 * Hook `wp_enqueue_scripts` handler.
	 */
	public function wp_enqueue_scripts(): void {
	}

	/**
	 * Hook `admin_enqueue_scripts` handler.
	 */
	public function admin_enqueue_scripts(): void {
	}
}
