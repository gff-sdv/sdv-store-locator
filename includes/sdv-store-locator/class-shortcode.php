<?php

declare( strict_types=1 );

namespace SDV_Store_Locator;

class Shortcode {

	protected Store_Locator_Options $options;

	/**
	 * Constructor.
	 */
	public function __construct( Store_Locator_Options $options ) {
		$this->options = $options;

		add_action( 'init', [ $this, 'init' ] );
	}

	/**
	 * Hook `init` handler.
	 *
	 * Initializes the shortcodes for the store-locator.
	 */
	public function init(): void {
		$google_maps_api_key = $this->options->get_option_google_maps_api_key();

		// Register the shortcode for the store-locator.
		add_shortcode( 'store-locator', [ $this, 'shortcode' ] );

		///
		// Register scripts and styles used in the frontend for the store-locator.
		///
		wp_register_script(
			'google-maps',
			'https://maps.googleapis.com/maps/api/js?key=' . urlencode( $google_maps_api_key ) . '&v=weekly&callback=Function.prototype',
			[],
			false,
			true
		);
		wp_register_script(
			'store-locator-autocomplete',
			SDV_STORE_LOCATOR_ROOT_URL . 'scripts/autocomplete.js',
			[],
			SDV_STORE_LOCATOR_VERSION
		);
		wp_register_script(
			'store-locator-view',
			SDV_STORE_LOCATOR_ROOT_URL . 'scripts/view.js',
			[ 'store-locator-autocomplete' ],
			SDV_STORE_LOCATOR_VERSION
		);
		wp_register_style(
			'store-locator-autocomplete',
			SDV_STORE_LOCATOR_ROOT_URL . 'styles/autocomplete.css',
			[],
			SDV_STORE_LOCATOR_VERSION
		);
		wp_register_style(
			'store-locator-view',
			SDV_STORE_LOCATOR_ROOT_URL . 'styles/view.css',
			[ 'store-locator-autocomplete' ],
			SDV_STORE_LOCATOR_VERSION
		);
	}

	/**
	 * Replaces the store-locator shortcode with the corresponding html contents
	 * and enqueues the scripts and styles required.
	 *
	 * Triggered by the presence of a shortcode in the contents.
	 *
	 * @return string
	 */
	public function shortcode(): string {
		///
		// Enqueue scripts and styles.
		///
		wp_enqueue_script( 'google-maps' );
		wp_enqueue_script( 'store-locator-view' );
		wp_enqueue_style( 'store-locator-view' );

		// Build the location url (used for the API requests to find locations by
		// zip code of location names).
		$location_url = add_query_arg( [
			'_nonce' => sdv_store_locator()->get_location_api()->create_nonce(),
			'action' => sdv_store_locator()->get_location_api()->get_action(),
		], admin_url( 'admin-ajax.php' ) );

		// Build the search url (used for the API requests to find stores).
		$search_url = add_query_arg( [
			'_nonce' => sdv_store_locator()->get_search_api()->create_nonce(),
			'action' => sdv_store_locator()->get_search_api()->get_action(),
		], admin_url( 'admin-ajax.php' ) );

		// Build a unique map id.
		$map_uid = wp_unique_id( 'map-' );

		return '<div class="store-locator" data-search-url="' . esc_attr__( $search_url ) . '" data-opening-hours="' .
		       __( 'Opening hours', 'store-locator' ) . '">
<div class="autocomplete-wrapper">
	<input type="search" spellcheck=false ' .
		       'placeholder="' . __( 'Postal Code / Location', 'store-locator' ) . '" ' .
		       'autocomplete="off" autocapitalize="off" maxlength="255" ' .
		       'data-autocomplete-url="' . esc_attr__( $location_url ) . '" ' .
		       'data-autocomplete-no-results="Nichts gefunden.">
</div>
<div id="' . esc_attr__( $map_uid ) . '" class="store-locator-map" data-map></div>
<div class="store-locator-results" data-results></div>
</div>
';
	}
}
