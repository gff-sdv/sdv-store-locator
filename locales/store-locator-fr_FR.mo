��          �      �       H  )   I     s     �  +   �     �     �  +   �          (     ?  L   Q     �     �  �  �  @   ?  	   �     �  4   �     �  %   �  9        O  0   b     �  L   �     �                
                             	                         GFF SDV store locator to find drugstores. GPS Locations Google Maps API Key Import geo locations from data.geo.admin.ch Last refresh: %s No opening hours available. Number of geo locations in the database: %s Opening hours Postal Code / Location SDV Store Locator SDV Store Locator needs the SDV Vendor plugin to be installed and activated. Store Locator closed Project-Id-Version: Store Locator 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/sdv-store-locator
PO-Revision-Date: 2022-06-17 10:58+0200
Last-Translator: Adrian Suter <adrian.suter@gff.ch>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.1
 GFF SDV Store Locator pour rechercher et trouver des drogueries. GPS Lieus Google Maps API Key Importez les géo-localisations de data.geo.admin.ch Dernière mise à jour : %s Pas d'heures d'ouverture disponibles. Nombre de géolocalisations dans la base de données : %s Heures d'ouverture Indiquez votre numéro postal ou votre localité SDV Recherche de droguerie SDV Store Locator needs the SDV Vendor plugin to be installed and activated. Recherche de droguerie fermé 