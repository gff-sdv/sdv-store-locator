��          �      �       H  )   I     s     �  +   �     �     �  +   �          (     ?  L   Q     �     �  �  �  0   b     �     �  1   �     �        +   $     P  '   `     �  A   �     �     �           
                             	                         GFF SDV store locator to find drugstores. GPS Locations Google Maps API Key Import geo locations from data.geo.admin.ch Last refresh: %s No opening hours available. Number of geo locations in the database: %s Opening hours Postal Code / Location SDV Store Locator SDV Store Locator needs the SDV Vendor plugin to be installed and activated. Store Locator closed Project-Id-Version: Store Locator 1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/sdv-store-locator
PO-Revision-Date: 2022-06-17 10:57+0200
Last-Translator: Adrian Suter <adrian.suter@gff.ch>
Language-Team: GFF Integrative Kommunikation GmbH
Language: de_CH
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1
 GFF SDV Drogeriesuche zum Suchen nach Drogerien. GPS Orte Google Maps API Schlüssel Geo-Ortschaften von data.geo.admin.ch importieren Letzte Aktualisierung: %s Keine Öffnungszeiten vorhanden. Anzahl Geo-Ortschaften in der Datenbank: %s Öffnungszeiten Geben Sie hier die PLZ oder den Ort ein SDV Drogeriesuche Das Plugin `SDV Drogeriesuche` benötigt das Plugin `SDV Vendor`. Drogeriesuche geschlossen 